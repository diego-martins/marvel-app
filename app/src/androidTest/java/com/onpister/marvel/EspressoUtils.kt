package com.onpister.marvel

import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.ViewAssertion
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.util.HumanReadables
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import android.support.design.widget.CollapsingToolbarLayout

class EspressoUtils {
    companion object {

        fun waitFor(condition: Matcher<View>, timeout: Long = 500): ViewAction {
            return waitAction(condition, timeout)
        }

        fun wait(timeout: Long = 1000): ViewAction {
            return waitAction(null, timeout)
        }

        fun waitAction(condition: Matcher<View>?, timeout: Long) = object : ViewAction {

            override fun getConstraints(): Matcher<View> {
                return Matchers.anything() as Matcher<View>
            }

            override fun getDescription(): String {
                return "wait $timeout"
            }

            override fun perform(uiController: UiController?, view: View?) {
                uiController?.loopMainThreadUntilIdle()
                val startTime = System.currentTimeMillis()
                val endTime = startTime + timeout

                while (System.currentTimeMillis() < endTime) {
                    if (condition != null && condition.matches(view)) {
                        return
                    }
                    uiController?.loopMainThreadForAtLeast(100)
                }
            }
        }

        fun clickOnViewChild(viewId: Int, tag: Any? = null) = object : ViewAction {
            override fun getConstraints() = null

            override fun getDescription() = "Click on a child view with specified id and/or tag."

            override fun perform(uiController: UiController?, view: View) {
                view.findViewById<View>(viewId)?.let { foundView ->
                    if(tag == null || foundView.tag == tag) {
                        foundView.performClick()
                    }
                }
            }
        }

        fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
            checkNotNull(itemMatcher)
            return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("has item at position $position: ")
                    itemMatcher.describeTo(description)
                }

                override fun matchesSafely(view: RecyclerView): Boolean {
                    val viewHolder = view.findViewHolderForAdapterPosition(position) ?: return false
                    return itemMatcher.matches(viewHolder.itemView)
                }
            }
        }

        fun checkSwipeRefresh(isRefreshing: Boolean): ViewAssertion {
            return ViewAssertion { view, _ ->
                if(view == null || view !is SwipeRefreshLayout || view.isRefreshing != isRefreshing){
                    throw AssertionError(HumanReadables.describe(view))
                }
            }
        }

        fun withCollapsibleToolbarTitle(textMatcher: Matcher<String>): Matcher<Any> {
            return object : BoundedMatcher<Any, CollapsingToolbarLayout>(CollapsingToolbarLayout::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("with toolbar title: ")
                    textMatcher.describeTo(description)
                }

                override fun matchesSafely(toolbarLayout: CollapsingToolbarLayout): Boolean {
                    return textMatcher.matches(toolbarLayout.title)
                }
            }
        }

    }
}
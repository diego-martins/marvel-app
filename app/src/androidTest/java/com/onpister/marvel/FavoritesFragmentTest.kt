package com.onpister.marvel

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.onpister.marvel.adapters.CharacterHolder
import org.junit.Test
import org.junit.runner.RunWith
import android.support.v7.widget.RecyclerView
import android.support.test.espresso.matcher.RootMatchers.isDialog
import org.hamcrest.Matchers.*
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import okhttp3.mockwebserver.MockResponse

@RunWith(AndroidJUnit4::class)
class FavoritesFragmentTest: BaseTest() {

    @Test
    fun testRemoveFavoriteAndCheckCharactersTab() {

        for(i in 1..2) {
            mockWebServer.enqueue(
                MockResponse()
                    .setResponseCode(200)
                    .setBody(getJson(json20Characters))
            )
        }
        activityRule.launchActivity(Intent())

        onView(withId(R.id.toolbar))
            .check(matches(hasDescendant(withText(R.string.tab_characters))))

        onView(withId(R.id.listViewCharacters))
            .perform(EspressoUtils.wait())
            .check(matches(isDisplayed()))

        onView(withId(R.id.listViewCharacters))
            .check(matches(EspressoUtils.atPosition(0, hasDescendant(withText(character1)))))
            .check(matches(EspressoUtils.atPosition(1, hasDescendant(withText(character2)))))

        onView(withId(R.id.listViewCharacters))
            .perform(RecyclerViewActions.actionOnItemAtPosition<CharacterHolder>(0,
                EspressoUtils.clickOnViewChild(R.id.btnFavorite, R.string.tag_no_favorite)))
            .perform(RecyclerViewActions.actionOnItemAtPosition<CharacterHolder>(1,
                EspressoUtils.clickOnViewChild(R.id.btnFavorite, R.string.tag_no_favorite)))

        onView(withId(R.id.container))
            .perform(swipeLeft())
            .perform(EspressoUtils.wait(3000))

        onView(withId(R.id.listViewFavorites))
            .perform(EspressoUtils.wait())
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))

        onView(withId(R.id.listViewFavorites))
            .check(matches(EspressoUtils.atPosition(0, hasDescendant(withText(character1)))))

        onView(withId(R.id.listViewFavorites))
            .perform(RecyclerViewActions.actionOnItemAtPosition<CharacterHolder>(0,
                EspressoUtils.clickOnViewChild(R.id.btnFavorite, R.string.tag_favorite)))

        onView(withText(R.string.btn_yes))
            .check(matches(isDisplayed()))
            .inRoot(isDialog())
            .perform(click())

        onView(isRoot()).perform(EspressoUtils.wait(2000))

        assertThat(getItemCount(R.id.listViewFavorites), greaterThanOrEqualTo(1))

    }

    @Test
    fun testOpenActivityCharacterDetail() {

        for(i in 1..2) {
            mockWebServer.enqueue(
                MockResponse()
                    .setResponseCode(200)
                    .setBody(getJson(json20Characters))
            )
        }
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(getJson(json01Characters)))

        activityRule.launchActivity(Intent())

        onView(withId(R.id.toolbar))
            .check(matches(hasDescendant(withText(R.string.tab_characters))))

        onView(withId(R.id.listViewCharacters))
            .perform(EspressoUtils.wait())
            .check(matches(isDisplayed()))

        onView(withId(R.id.listViewCharacters))
            .check(matches(EspressoUtils.atPosition(0, hasDescendant(withText(character1)))))
            .check(matches(EspressoUtils.atPosition(1, hasDescendant(withText(character2)))))

        onView(withId(R.id.listViewCharacters))
            .perform(RecyclerViewActions.actionOnItemAtPosition<CharacterHolder>(0,
                EspressoUtils.clickOnViewChild(R.id.btnFavorite, R.string.tag_no_favorite)))
            .perform(RecyclerViewActions.actionOnItemAtPosition<CharacterHolder>(1,
                EspressoUtils.clickOnViewChild(R.id.btnFavorite, R.string.tag_no_favorite)))

        onView(withId(R.id.container))
            .perform(swipeLeft())
            .perform(EspressoUtils.wait(3000))

        onView(withId(R.id.listViewFavorites))
            .perform(EspressoUtils.wait())
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))

        onView(withId(R.id.listViewFavorites))
            .perform(RecyclerViewActions.actionOnItemAtPosition<CharacterHolder>(0,
                EspressoUtils.clickOnViewChild(R.id.imgThumb)
            ))

        onView(isRoot()).perform(EspressoUtils.wait(2000))

        onView(withId(R.id.toolbar_layout)).check(matches(EspressoUtils.withCollapsibleToolbarTitle(`is`(character1))))

        onView(isRoot()).perform(ViewActions.pressBack())
            .perform(EspressoUtils.wait(2000))
    }

}
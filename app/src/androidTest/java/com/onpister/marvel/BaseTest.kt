package com.onpister.marvel

import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.onpister.marvel.activities.MainActivity
import com.onpister.marvel.adapters.CharacterAdapter
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
open class BaseTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    val character1 = "3-D Man"
    val character2 = "A-Bomb (HAS)"
    val json0Characters = "/0_character.json"
    val json01Characters = "/01_character.json"
    val json20Characters = "/20_characters.json"

    lateinit var mockWebServer: MockWebServer

    @Before
    @Throws(Exception::class)
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start(8080)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testDefaultTab() {
        activityRule.launchActivity(Intent())
        Espresso.onView(ViewMatchers.withId(R.id.toolbar))
            .check(ViewAssertions.matches(ViewMatchers.hasDescendant(ViewMatchers.withText(R.string.tab_characters))))
    }

    fun getJson(filename : String) : String {
        val res = javaClass.getResource(filename)
        return res.readText()
    }

    fun getItemCount(id: Int): Int {
        val listViewCharacters = activityRule.activity.findViewById<RecyclerView>(id)
        if(listViewCharacters != null && !(listViewCharacters.adapter as CharacterAdapter).isEmpty()) {
            return listViewCharacters.adapter.itemCount
        }
        return 0
    }
}
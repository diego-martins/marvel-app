package com.onpister.marvel.fragments

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import com.onpister.marvel.activities.BaseApp
import com.onpister.marvel.viewmodels.CharactersViewModel
import com.onpister.marvel.viewmodels.CharactersViewModelFactory
import javax.inject.Inject

open class BaseFragment: Fragment() {

    @Inject
    lateinit var charactersViewModelFactory: CharactersViewModelFactory
    protected lateinit var viewModel: CharactersViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        BaseApp.appComponent().inject(this)
        viewModel = ViewModelProviders
            .of(activity!!, charactersViewModelFactory)
            .get(CharactersViewModel::class.java)
    }

}
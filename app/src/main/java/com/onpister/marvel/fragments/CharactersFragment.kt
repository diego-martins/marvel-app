package com.onpister.marvel.fragments

import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import com.onpister.marvel.activities.CharacterDetailActivity
import com.onpister.marvel.R
import com.onpister.marvel.Utils
import com.onpister.marvel.adapters.CharacterAdapter
import com.onpister.marvel.adapters.GridSpacingItemDecoration
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.data.remote.NoConnectivityException
import com.onpister.marvel.isVisible
import com.onpister.marvel.visible
import kotlinx.android.synthetic.main.characters_fragment.*
import org.jetbrains.anko.toast

class CharactersFragment : BaseFragment() {

    private var adapter: CharacterAdapter? = null
    private var searchView: SearchView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.characters_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        init()
    }

    private fun init() {
        adapter = CharacterAdapter()
        listViewCharacters.adapter = adapter
        val gridColumns = activity!!.resources.getInteger(R.integer.grid_columns)
        val gridLayout = GridLayoutManager(activity!!, gridColumns)
        gridLayout.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int =
                if (adapter!!.getItemViewType(position) == R.layout.item_loading) gridColumns else 1
        }

        listViewCharacters.layoutManager = gridLayout
        listViewCharacters.addItemDecoration(
            GridSpacingItemDecoration(gridColumns, 30, true)
        )
        loadEvents()
    }

    private fun loadEvents() {

        viewModel.getCharacters().observe(activity!!, Observer { list ->
            list?.let {
                if(it.isEmpty()) {
                    listViewCharacters.visible(false)
                    if(!Utils.isOnline(activity!!) && adapter!!.isEmpty()) {
                        emptyResult.visible(false)
                        offline.visible(true)
                    } else {
                        offline.visible(false)
                        emptyResult.visible(true)
                    }
                } else {
                    adapter!!.submitList(it)
                    emptyResult.visible(false)
                    offline.visible(false)
                    listViewCharacters.visible(true)
                }
                textTop.visible(false)
                refresh.isRefreshing = false
            }
        })

        viewModel.isLoadingCharacter().observe(activity!!, Observer {
            adapter!!.setLoading(it ?: false)
        })

        viewModel.getRefresh().observe(activity!!, Observer {
            refresh.isRefreshing = it ?: false
        })

        refresh.setOnRefreshListener {
            if(Utils.isOnline(activity!!)) {
                viewModel.invalidateCharacterDataSource()
            } else {
                refresh.isRefreshing = false
                val errorResource = Utils.getCustomError(NoConnectivityException())
                if(errorResource > 0) {
                    activity?.toast(errorResource)
                }
            }
        }

        adapter!!.setClickItemListener {
            character: Character ->
                val intent = Intent(activity!!, CharacterDetailActivity::class.java)
                intent.putExtra(CharacterDetailActivity.PARAM_CHARACTER_ID, character.id)
                startActivityForResult(intent, REQ_CHARACTER_DETAIL)
        }

        adapter!!.setClickFavListener {
            item ->
                if(item.favorite) {
                    val builder = AlertDialog.Builder(activity!!)
                    builder.setMessage(activity!!.getString(R.string.confirm_remove_favorite, item.name))
                    builder.setPositiveButton(R.string.btn_yes) { _, _ -> viewModel.removeFavorite(item) }
                    builder.setNegativeButton(R.string.btn_no, null)
                    val alert = builder.create()
                    alert.show()
                } else {
                    viewModel.addFavorite(item)
                }
        }

        viewModel.getCurrentCharacter().observe(activity!!, Observer {
            adapter!!.updateItem(it!!)
        })

        viewModel.getOnline().observe(activity!!, Observer {
            if(it == false) {
                textTop.visible(false)
                activity?.toast(R.string.error_offline)
            } else if(offline.isVisible()) {
                viewModel.invalidateCharacterDataSource()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (menu != null) {
            inflater!!.inflate(R.menu.menu_main, menu)
            val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView = menu.findItem(R.id.action_search).actionView as SearchView
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
            searchView!!.queryHint = activity!!.getString(R.string.hint_search)
            searchView!!.isSubmitButtonEnabled = true
            searchView!!.setOnCloseListener {
                viewModel.searchText(null)
                false
            }
            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    if(viewModel.getOnline().value == true) {
                        if (query.isEmpty()) {
                            viewModel.searchText(null)
                        } else {
                            textTop.visible(true)
                            viewModel.searchText(query)
                        }
                        return true
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean = false
            })
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQ_CHARACTER_DETAIL && resultCode == CharacterDetailActivity.RESULT_FAVORITE_CHANGED) {
            viewModel.invalidateCharacterDataSource()
            viewModel.invalidateFavoritesDataSource()
        }
    }

    companion object {
        private const val REQ_CHARACTER_DETAIL= 1
    }

}

package com.onpister.marvel.fragments

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.view.*
import com.onpister.marvel.activities.CharacterDetailActivity
import com.onpister.marvel.R
import com.onpister.marvel.adapters.CharacterAdapter
import com.onpister.marvel.adapters.GridSpacingItemDecoration
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.visible
import kotlinx.android.synthetic.main.favorites_fragment.*

class FavoritesFragment : BaseFragment() {

    private var adapter: CharacterAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.favorites_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        init()
    }

    private fun init() {
        adapter = CharacterAdapter()
        listViewFavorites.adapter = adapter
        val gridColumns = activity!!.resources.getInteger(R.integer.grid_columns)
        val gridLayout = GridLayoutManager(activity!!, gridColumns)
        gridLayout.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int =
                if (adapter!!.getItemViewType(position) == R.layout.item_loading) gridColumns else 1
        }

        listViewFavorites.layoutManager = gridLayout
        listViewFavorites.addItemDecoration(
            GridSpacingItemDecoration(gridColumns, 30, true)
        )
        loadEvents()
    }

    private fun loadEvents() {

        viewModel.getFavorites().observe(activity!!, Observer { list ->
            list?.let {
                if(it.isEmpty() && adapter!!.isEmpty()) {
                    listViewFavorites.visible(false)
                    noFavorites.visible(true)
                } else {
                    adapter!!.submitList(it)
                    noFavorites.visible(false)
                    listViewFavorites.visible(true)
                }
                textTop.visible(false)
            }
        })

        viewModel.isLoadingFavorite().observe(activity!!, Observer {
            adapter!!.setLoading(it ?: false)
        })

        adapter!!.setClickItemListener {
                character: Character ->
            val intent = Intent(activity!!, CharacterDetailActivity::class.java)
            intent.putExtra(CharacterDetailActivity.PARAM_CHARACTER_ID, character.id)
            startActivityForResult(intent, REQ_CHARACTER_DETAIL)
        }

        adapter!!.setClickFavListener {
            item ->
            if(item.favorite) {
                val builder = AlertDialog.Builder(activity!!)
                builder.setMessage(activity!!.getString(R.string.confirm_remove_favorite, item.name))
                builder.setPositiveButton(R.string.btn_yes) { _, _ -> viewModel.removeFavorite(item) }
                builder.setNegativeButton(R.string.btn_no, null)
                val alert = builder.create()
                alert.show()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQ_CHARACTER_DETAIL && resultCode == CharacterDetailActivity.RESULT_FAVORITE_CHANGED) {
            viewModel.invalidateFavoritesDataSource()
            viewModel.invalidateCharacterDataSource()
        }
    }

    companion object {
        private const val REQ_CHARACTER_DETAIL = 1
    }

}

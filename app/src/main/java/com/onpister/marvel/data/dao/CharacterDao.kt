package com.onpister.marvel.data.dao

import android.arch.persistence.room.*
import com.onpister.marvel.data.models.Character
import io.reactivex.Single

@Dao
interface CharacterDao {

    @Query("SELECT * FROM characters WHERE id = :id")
    fun get(id: Int): Single<Character>

    @Query("SELECT * FROM characters ORDER BY name LIMIT :offset, :limit")
    fun getAll(offset: Int, limit: Int): Single<List<Character>>

    @Query("SELECT * FROM characters WHERE id IN (:ids) ORDER BY name")
    fun getAllByIds(ids: List<Int>): Single<List<Character>>

    @Query("SELECT * FROM characters WHERE name LIKE :name || '%' ORDER BY name LIMIT :offset, :limit")
    fun getAllBySearch(name: String, offset: Int, limit: Int): Single<List<Character>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(character: Character): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(character: Character): Int

    @Delete
    fun delete(character: Character): Int

}
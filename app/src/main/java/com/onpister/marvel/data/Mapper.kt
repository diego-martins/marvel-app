package com.onpister.marvel.data

import com.onpister.marvel.data.models.Character
import com.onpister.marvel.data.models.Comic
import com.onpister.marvel.data.models.Serie
import com.onpister.marvel.data.remote.ApiResponse

class Mapper {
    companion object {

        fun remoteToLocalComic(remote: ApiResponse.Comic): Comic {
            val local = Comic()
            local.id = remote.id
            local.title = remote.title
            local.description = remote.description ?: ""
            local.modified = remote.modified
            remote.thumbnail?.let {
                local.thumbnailPath = it.path
                local.thumbnailExtension = it.extension
            }
            val detailUrl = remote.urls.find { url -> url.type == "detail" }
            if(detailUrl != null) {
                local.detailUrl = detailUrl.url
            }
            return local
        }

        fun remoteToLocalComics(remote: List<ApiResponse.Comic>): List<Comic> {
            val local = ArrayList<Comic>()
            remote.forEach {
                local.add(remoteToLocalComic(it))
            }
            return local
        }

        fun remoteToLocalSerie(remote: ApiResponse.Serie): Serie {
            val local = Serie()
            local.id = remote.id
            local.title = remote.title
            local.description = remote.description ?: ""
            local.modified = remote.modified
            remote.thumbnail?.let {
                local.thumbnailPath = it.path
                local.thumbnailExtension = it.extension
            }
            val detailUrl = remote.urls.find { url -> url.type == "detail" }
            if(detailUrl != null) {
                local.detailUrl = detailUrl.url
            }
            return local
        }

        fun remoteToLocalSeries(remote: List<ApiResponse.Serie>): List<Serie> {
            val local = ArrayList<Serie>()
            remote.forEach {
                local.add(remoteToLocalSerie(it))
            }
            return local
        }

        fun remoteToLocalCharacter(remote: ApiResponse.Character): Character {
            val local = Character()
            local.id = remote.id
            local.name = remote.name
            local.description = remote.description ?: ""
            local.modified = remote.modified
            remote.thumbnail?.let {
                local.thumbnailPath = it.path
                local.thumbnailExtension = it.extension
            }
            return local
        }

        fun remoteToLocalCharacters(remote: List<ApiResponse.Character>): List<Character> {
            val local = ArrayList<Character>()
            remote.forEach {
                local.add(remoteToLocalCharacter(it))
            }
            return local
        }
    }
}
package com.onpister.marvel.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "characters")
open class Character {
    @PrimaryKey
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var modified: String = ""
    var thumbnailPath: String = ""
    var thumbnailExtension: String = ""
    var favorite: Boolean = false
}
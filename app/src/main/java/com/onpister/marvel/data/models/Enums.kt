package com.onpister.marvel.data.models

enum class Tabs {
    CHARACTERS,
    FAVORITES
}
package com.onpister.marvel.data.models

class FlexMessage {
    var res: Int = 0
    var message: String = ""

    constructor(res: Int) {
        this.res = res
    }

    constructor(message: String) {
        this.message = message
    }
}
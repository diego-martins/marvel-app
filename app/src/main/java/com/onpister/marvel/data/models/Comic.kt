package com.onpister.marvel.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "comics")
open class Comic {
    @PrimaryKey
    var id: Int = 0
    var title: String = ""
    var description: String = ""
    var modified: String = ""
    var thumbnailPath: String = ""
    var thumbnailExtension: String = ""
    var detailUrl: String = ""
}
package com.onpister.marvel.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("/v1/public/characters")
    fun getCharacters(
        @Query("offset") offset: Int = 0,
        @Query("limit") limit: Int = 20)
            : Single<ApiResponse.MarvelBase<ApiResponse.Character>>

    @GET("/v1/public/characters")
    fun getCharacters(
        @Query("nameStartsWith") name: String = "",
        @Query("offset") offset: Int = 0,
        @Query("limit") limit: Int = 20)
            : Single<ApiResponse.MarvelBase<ApiResponse.Character>>

    @GET("/v1/public/characters/{id}")
    fun getCharacter(@Path("id") id: Int): Single<ApiResponse.MarvelBase<ApiResponse.Character>>

    @GET("/v1/public/characters/{id}/comics")
    fun getCharacterComics(
        @Path("id") characterId: Int,
        @Query("offset") offset: Int = 0,
        @Query("limit") limit: Int = 20)
            : Single<ApiResponse.MarvelBase<ApiResponse.Comic>>

    @GET("/v1/public/characters/{id}/series")
    fun getCharacterSeries(
        @Path("id") characterId: Int,
        @Query("offset") offset: Int = 0,
        @Query("limit") limit: Int = 20)
            : Single<ApiResponse.MarvelBase<ApiResponse.Serie>>
}

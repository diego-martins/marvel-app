package com.onpister.marvel.data

import com.onpister.marvel.data.dao.CharacterDao
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.data.models.Comic
import com.onpister.marvel.data.models.Serie
import com.onpister.marvel.data.remote.ApiInterface
import com.onpister.marvel.data.remote.ApiResponse
import io.reactivex.Single
import javax.inject.Inject

class MarvelRepo
@Inject constructor(
    private var api: ApiInterface,
    private var characterDao: CharacterDao
) {

    fun getAll(offset: Int, limit: Int): Single<List<Character>> {
        return api.getCharacters(offset, limit).flatMap { result ->
            val results = Mapper.remoteToLocalCharacters(result.data.results)
            val ids = ArrayList<Int>()
            results.forEach {
                ids.add(it.id)
            }
            getAllFavoritesByIds(ids).flatMap {
                favorites ->
                    favorites.forEach {
                        favoriteItem ->
                            val resultItem = results.find { it.id == favoriteItem.id }
                            if(resultItem != null) {
                                resultItem.favorite = favoriteItem.favorite
                            }
                    }
                    Single.just(results)
            }
        }
    }

    fun getAllComics(characterId: Int, offset: Int, limit: Int): Single<List<Comic>> {
        return api.getCharacterComics(characterId, offset, limit).flatMap {
            val results = Mapper.remoteToLocalComics(it.data.results)
                Single.just(results)
        }
    }

    fun getAllSeries(characterId: Int, offset: Int, limit: Int): Single<List<Serie>> {
        return api.getCharacterSeries(characterId, offset, limit).flatMap {
            val results = Mapper.remoteToLocalSeries(it.data.results)
            Single.just(results)
        }
    }

    fun getAllBySearch(name: String, offset: Int, limit: Int): Single<List<Character>> {
        if(name.isEmpty()) return Single.just(emptyList())
        return api.getCharacters(name, offset, limit).map {
            Mapper.remoteToLocalCharacters(it.data.results)
        }
    }

    fun get(id: Int): Single<Character?> {
        return getFavorite(id).flatMap {
            Single.just(it)
        }.onErrorResumeNext{
            api.getCharacter(id).map {
                apiResult: ApiResponse.MarvelBase<ApiResponse.Character> ->
                if(apiResult.data.results.isNotEmpty()) {
                    Mapper.remoteToLocalCharacter(apiResult.data.results[0])
                } else {
                    null
                }
            }
        }
    }

    fun getFavorite(id: Int): Single<Character> {
        return characterDao.get(id)
    }

    fun getAllFavoritesByIds(ids: List<Int>): Single<List<Character>> {
        return characterDao.getAllByIds(ids)
    }

    fun getAllFavorites(offset: Int, limit: Int): Single<List<Character>> {
        return characterDao.getAll(offset, limit)
    }

    fun upsertFavorite(character: Character): Single<Long> {
        return Single.fromCallable {
            var result: Long = characterDao.insert(character)
            if(result == 0L) {
                result = characterDao.update(character).toLong()
            }
            result
        }
    }

    fun deleteFavorite(character: Character): Single<Int> {
        return Single.fromCallable {
            characterDao.delete(character)
        }
    }
}
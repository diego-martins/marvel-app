package com.onpister.marvel.data.remote

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.onpister.marvel.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String?
        get() = "No network available, please check your WiFi or Data connection"
}

class APIClient(private val httpClient: OkHttpClient) {
    private var retrofit: Retrofit? = null

    val client: Retrofit
        get() {

            retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.apiUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()

            return retrofit as Retrofit
        }
}

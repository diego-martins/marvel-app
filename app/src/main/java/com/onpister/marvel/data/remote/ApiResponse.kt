package com.onpister.marvel.data.remote

class ApiResponse {

    data class Url(
            var type: String,
            var url: String
    )

    data class Thumbnail(
            var path: String,
            var extension: String
    )

    data class Character(
        var id: Int = 0,
        var name: String = "",
        var description: String? = null,
        var modified: String = "",
        var urls: List<Url> = emptyList(),
        var thumbnail: Thumbnail? = null
    )

    data class Comic(
        var id: Int = 0,
        var title: String = "",
        var description: String? = null,
        var thumbnail: Thumbnail? = null,
        var urls: List<Url> = emptyList(),
        var modified: String = ""
    )

    data class Serie(
        var id: Int = 0,
        var title: String = "",
        var description: String? = null,
        var thumbnail: Thumbnail? = null,
        var urls: List<Url> = emptyList(),
        var modified: String = ""
    )

    data class MarvelData<T>(
        var offset: Int = 0,
        var limit: Int = 0,
        var total: Int = 0,
        var count: Int = 0,
        var results: List<T> = emptyList()
    )

    data class MarvelBase<T>(
        var code: Int = 0,
        var status: String = "",
        var copyright: String = "",
        var attributionText: String = "",
        var attributionHTML: String = "",
        var data: MarvelData<T> = MarvelData()
    )
}
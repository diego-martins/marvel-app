package com.onpister.marvel

import android.content.Context
import android.net.ConnectivityManager
import com.onpister.marvel.data.remote.NoConnectivityException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class Utils {
    companion object {
        fun isOnline(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }

        fun getCustomError(t: Throwable): Int = when(t) {
            is UnknownHostException ->
                R.string.error_server
            is SocketTimeoutException ->
                R.string.error_timeout
            is NoConnectivityException ->
                R.string.error_offline
            else -> 0
        }
    }
}
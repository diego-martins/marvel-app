package com.onpister.marvel

import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import java.lang.Exception

fun View.visible(visible: Boolean?) {
    this.visibility = if(visible == true) View.VISIBLE else View.GONE
}

fun View.isVisible() = this.visibility == View.VISIBLE

fun ImageView.loadUrl(url: String?) {
    if(!url.isNullOrEmpty()){
        val target = this
        Picasso.get()
            .load(url)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(target, object: Callback{
                override fun onSuccess() {
                }

                override fun onError(e: Exception?) {
                    Picasso.get().load(url).into(target)
                }
            })
    }
}

package com.onpister.marvel.activities

import android.app.Application
import com.onpister.marvel.di.*

open class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule(this))
            .databaseModule(DatabaseModule(this))
            .build()
        component.inject(this)
    }

    companion object {
        lateinit var component: AppComponent

        fun appComponent(): AppComponent = component
    }
}
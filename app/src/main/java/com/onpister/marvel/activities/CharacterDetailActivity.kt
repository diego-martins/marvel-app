package com.onpister.marvel.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.*
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.onpister.marvel.R
import com.onpister.marvel.Utils
import com.onpister.marvel.adapters.ComicAdapter
import com.onpister.marvel.adapters.SerieAdapter
import com.onpister.marvel.viewmodels.CharacterDetailViewModel
import com.onpister.marvel.viewmodels.CharactersViewModelFactory
import com.onpister.marvel.visible
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_character_detail.*
import kotlinx.android.synthetic.main.content_character_detail.*
import org.jetbrains.anko.toast
import java.lang.Exception
import javax.inject.Inject
import android.text.method.LinkMovementMethod
import com.onpister.marvel.loadUrl

class CharacterDetailActivity : AppCompatActivity() {

    companion object {
        const val PARAM_CHARACTER_ID = "CHARACTER_ID"
        const val RESULT_FAVORITE_CHANGED = 1
    }

    @Inject
    lateinit var charactersViewModelFactory: CharactersViewModelFactory
    private lateinit var viewModel: CharacterDetailViewModel
    private var comicAdapter: ComicAdapter? = null
    private var serieAdapter: SerieAdapter? = null
    private var networkReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(context != null) {
                viewModel.setOnline(Utils.isOnline(context))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_detail)
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        BaseApp.appComponent().inject(this)

        viewModel = ViewModelProviders
            .of(this, charactersViewModelFactory)
            .get(CharacterDetailViewModel::class.java)

        init()
    }

    private fun init() {
        val characterId = intent.extras.getInt(PARAM_CHARACTER_ID, 0)
        if(characterId == 0) {
            this.toast(R.string.error_character_not_found)
            finish()
            return
        }

        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL)
        itemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)

        //caracter
        viewModel.loadCharacter(characterId)

        //comics
        comicAdapter = ComicAdapter()
        listViewComics.adapter = comicAdapter
        listViewComics.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        listViewComics.addItemDecoration(itemDecoration)
        viewModel.loadComics(characterId)

        //series
        serieAdapter = SerieAdapter()
        listViewSeries.adapter = serieAdapter
        listViewSeries.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        listViewSeries.addItemDecoration(itemDecoration)
        viewModel.loadSeries(characterId)

        textMarvelAttribution.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(getString(R.string.marvel_attribution_url))
            startActivity(intent)
        }

        loadEvents()
    }

    private fun showDetail(title: String, description: String, url: String) {
        if(url.isNotEmpty()) {
            val dialog = AlertDialog.Builder(this)
            dialog.setTitle(title)
            if(description.isNotEmpty()) {
                if(description.length > 300) {
                    dialog.setMessage(description.substring(0, 300).plus("..."))
                } else {
                    dialog.setMessage(description)
                }
            } else {
                dialog.setMessage(R.string.label_no_description)
            }
            dialog.setNegativeButton(R.string.btn_close, null)
            dialog.setPositiveButton(R.string.btn_open_in_browser) { dialogInterface, _ ->
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
                dialogInterface.dismiss()
            }
            dialog.show()
        }
    }

    private fun loadEvents() {
        viewModel.getCharacter().observe(this, Observer {
            character ->
                character?.let {
                    toolbar_layout?.title = it.name
                    toolbar?.title = it.name
                    textDescription.text = if(it.description.isEmpty()) getString(R.string.label_no_description) else it.description
                    val imgUrlPreview = "${it.thumbnailPath}/standard_xlarge.${it.thumbnailExtension}"
                    val imgUrlDetail = "${it.thumbnailPath}/detail.${it.thumbnailExtension}"
                    Picasso.get().load(imgUrlDetail).into(imgThumb, object: Callback {
                        override fun onSuccess() {
                            progressImage.visible(false)
                        }
                        override fun onError(e: Exception?) {
                            imgThumb.loadUrl(imgUrlPreview)
                            progressImage.visible(false)
                        }
                    })
                    invalidateOptionsMenu()
                }
        })

        viewModel.getComics().observe(this, Observer { list ->
            val hasComics = list != null && list.isNotEmpty()
            textComics.visible(!hasComics && comicAdapter!!.isEmpty())
            listViewComics.visible(hasComics || !comicAdapter!!.isEmpty())
            if(hasComics) comicAdapter!!.submitList(list)
        })

        comicAdapter!!.setClickItemListener {
            showDetail(it.title, it.description, it.detailUrl)
        }

        serieAdapter!!.setClickItemListener {
            showDetail(it.title, it.description, it.detailUrl)
        }

        viewModel.getSeries().observe(this, Observer { list ->
            val hasSeries = list != null && list.isNotEmpty()
            textSeries.visible(!hasSeries && serieAdapter!!.isEmpty())
            listViewSeries.visible(hasSeries || !serieAdapter!!.isEmpty())
            if(hasSeries) serieAdapter!!.submitList(list)
        })

        viewModel.getComicsLoading().observe(this, Observer {
            comicAdapter!!.setLoading(it ?: false)
        })

        viewModel.getSeriesLoading().observe(this, Observer {
            serieAdapter!!.setLoading(it ?: false)
        })

        viewModel.getLoading().observe(this, Observer {
            isLoading: Boolean? ->
                isLoading?.let{
                    loading.visible(isLoading)
                }
        })

        viewModel.getError().observe(this, Observer {
            it?.let {
                val errorResource = Utils.getCustomError(it)
                if(errorResource > 0) {
                    this.toast(errorResource)
                }
            }
        })

        viewModel.getComicsError().observe(this, Observer {
            it?.let {
                val errorResource = Utils.getCustomError(it)
                if(errorResource > 0) {
                    this.toast(errorResource)
                } else {
                    this.toast(R.string.error_get_comics)
                }
            }
        })

        viewModel.getSeriesError().observe(this, Observer {
            it?.let {
                val errorResource = Utils.getCustomError(it)
                if(errorResource > 0) {
                    this.toast(errorResource)
                } else {
                    this.toast(R.string.error_get_series)
                }
            }
        })

        viewModel.getMessage().observe(this, Observer {
            it?.let {
                if(it.res > 0) {
                    this.toast(it.res)
                } else {
                    this.toast(it.message)
                }
            }
        })

        viewModel.getFinish().observe(this, Observer {
            if(it == true) {
                finish()
            }
        })

        viewModel.getOnline().observe(this, Observer {
            if(it == true) {
                if(comicAdapter!!.itemCount == 0) {
                    viewModel.invalidateComics()
                }

                if(serieAdapter!!.itemCount == 0) {
                    viewModel.invalidateSeries()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.let {
            menuInflater.inflate(R.menu.menu_character_detail, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if(menu != null) {
            viewModel.getCharacter().value?.let {
                if(it.favorite) {
                    menu.findItem(R.id.action_favorite).setIcon(R.drawable.ic_star)
                } else {
                    menu.findItem(R.id.action_favorite).setIcon(R.drawable.ic_star_outline)
                }
            }
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item != null) {
            when(item.itemId) {
                android.R.id.home -> {
                    if(viewModel.isFavoriteChanged()) {
                        setResult(RESULT_FAVORITE_CHANGED)
                    }
                    finish()
                }
                R.id.action_favorite -> {
                    viewModel.getCharacter().value?.let { character ->
                        if(character.favorite) {
                            val builder = AlertDialog.Builder(this)
                            builder.setMessage(getString(R.string.confirm_remove_favorite, character.name))
                            builder.setPositiveButton(R.string.btn_yes) { _, _ -> viewModel.removeFavorite() }
                            builder.setNegativeButton(R.string.btn_no, null)
                            val alert = builder.create()
                            alert.show()
                        } else {
                            viewModel.addFavorite()
                        }
                        return true
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(networkReceiver)
    }

}

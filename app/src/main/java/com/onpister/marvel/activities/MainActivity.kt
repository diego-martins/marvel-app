package com.onpister.marvel.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.support.design.widget.TabLayout

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.os.Bundle
import android.os.Handler
import com.onpister.marvel.fragments.CharactersFragment
import com.onpister.marvel.fragments.FavoritesFragment
import com.onpister.marvel.R
import com.onpister.marvel.Utils
import com.onpister.marvel.data.models.Tabs
import com.onpister.marvel.viewmodels.CharactersViewModel
import com.onpister.marvel.viewmodels.CharactersViewModelFactory

import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import javax.inject.Inject

class MainActivity : BaseActivity() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private lateinit var viewModel: CharactersViewModel
    private var networkReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(context != null) {
                viewModel.setOnline(Utils.isOnline(context))
            }
        }
    }

    @Inject
    lateinit var charactersViewModelFactory: CharactersViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        BaseApp.appComponent().inject(this)
        viewModel = ViewModelProviders
            .of(this, charactersViewModelFactory)
            .get(CharactersViewModel::class.java)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        container.adapter = mSectionsPagerAdapter
        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        tabs.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let{
                    viewModel.setTab(Tabs.values()[tab.position])
                }
            }
        })

        init()
    }

    private fun init() {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tabs.elevation = 0f
        }

        viewModel.getTab().observe(this, Observer {
            it?.let {
                when (it) {
                    Tabs.CHARACTERS -> {
                        toolbar.title = getString(R.string.tab_characters)
                    }
                    Tabs.FAVORITES -> {
                        toolbar.title = getString(R.string.tab_favorites)
                        // ensures to clear search text after change tab if text is diff
                        viewModel.searchText(null)
                    }
                }
                Handler().postDelayed({
                    viewModel.invalidatePendingDataSource()
                }, 300)
            }
        })

        viewModel.getError().observe(this, Observer {
            it?.let {
                val errorResource = Utils.getCustomError(it)
                if(errorResource > 0) {
                    this.toast(errorResource)
                }
            }
        })

        viewModel.getMessage().observe(this, Observer {
            it?.let {
                if(it.res > 0) {
                    this.toast(it.res)
                } else {
                    this.toast(it.message)
                }
            }
        })
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment = when (position) {
                0 -> CharactersFragment()
                else -> FavoritesFragment()
        }

        override fun getCount(): Int = Tabs.values().size
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(networkReceiver)
    }

}

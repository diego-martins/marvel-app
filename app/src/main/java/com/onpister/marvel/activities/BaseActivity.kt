package com.onpister.marvel.activities

import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity()
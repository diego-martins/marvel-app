package com.onpister.marvel.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import com.onpister.marvel.data.models.Serie
import com.onpister.marvel.loadUrl
import kotlinx.android.synthetic.main.item_serie.view.*

class SerieHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(item: Serie) {
        itemView.textTitle.text = item.title
        val imgUrl = "${item.thumbnailPath}/standard_large.${item.thumbnailExtension}"
        itemView.imgThumb.loadUrl(imgUrl)
    }

}
package com.onpister.marvel.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import com.onpister.marvel.R
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.loadUrl
import kotlinx.android.synthetic.main.item_character.view.*

class CharacterHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindTo(item: Character) {
        itemView.textName.text = item.name
        val imgUrl = "${item.thumbnailPath}/standard_xlarge.${item.thumbnailExtension}"
        itemView.imgThumb.loadUrl(imgUrl)
        if(item.favorite) {
            itemView.btnFavorite.setImageResource(R.drawable.ic_star)
            itemView.btnFavorite.tag = R.string.tag_favorite
            itemView.textName.tag = R.string.tag_favorite
        } else {
            itemView.btnFavorite.setImageResource(R.drawable.ic_star_outline)
            itemView.btnFavorite.tag = R.string.tag_no_favorite
            itemView.textName.tag = R.string.tag_favorite
        }
    }
}
package com.onpister.marvel.adapters

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.onpister.marvel.R
import com.onpister.marvel.data.models.Comic

typealias ClickComicListener = (Comic) -> Unit

class ComicAdapter : PagedListAdapter<Comic, RecyclerView.ViewHolder>(diffCallback) {

    private var clickItemListener: ClickComicListener? = null
    private var loading: Boolean = true

    fun setClickItemListener(click: ClickComicListener) {
        this.clickItemListener = click
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = ComicHolder(
        LayoutInflater
            .from(parent.context)
            .inflate(viewType, parent, false)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == R.layout.item_comic) {
            val item = getItem(position)
            if(item != null && getItemViewType(position) == R.layout.item_comic) {
                holder as ComicHolder
                holder.bindTo(item)
                holder.itemView.setOnClickListener {
                    if(clickItemListener != null) clickItemListener!!(item)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if(loading) 1 else 0
    }

    fun isEmpty(): Boolean =
        itemCount == 0 || getItemViewType(0) == R.layout.item_loading

    override fun getItemViewType(position: Int): Int =
        if (position < super.getItemCount()) R.layout.item_comic else R.layout.item_loading

    fun setLoading(loading: Boolean) {
        this.loading = loading
        notifyItemChanged(super.getItemCount())
    }

    companion object {
        val diffCallback = object: DiffUtil.ItemCallback<Comic>() {
            override fun areItemsTheSame(oldItem: Comic?, newItem: Comic?): Boolean {
                return newItem?.id == oldItem?.id
            }

            override fun areContentsTheSame(oldItem: Comic?, newItem: Comic?): Boolean {
                return newItem == oldItem
            }
        }
    }

}

package com.onpister.marvel.adapters

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.onpister.marvel.R
import com.onpister.marvel.data.models.Character
import kotlinx.android.synthetic.main.item_character.view.*

typealias ClickCharacterListener = (Character) -> Unit

class CharacterAdapter : PagedListAdapter<Character, RecyclerView.ViewHolder>(diffCallback) {

    private var clickItemListener: ClickCharacterListener? = null
    private var clickFavListener: ClickCharacterListener? = null
    private var loading: Boolean = true

    fun setClickItemListener(click: ClickCharacterListener) {
        this.clickItemListener = click
    }

    fun setClickFavListener(click: ClickCharacterListener) {
        this.clickFavListener = click
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = CharacterHolder(
        LayoutInflater
            .from(parent.context)
            .inflate(viewType, parent, false)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == R.layout.item_character) {
            val item = getItem(position)
            if(item != null && getItemViewType(position) == R.layout.item_character) {
                holder as CharacterHolder
                holder.bindTo(item)
                holder.itemView.imgThumb.setOnClickListener {
                    if(clickItemListener != null) clickItemListener!!(item)
                }
                holder.itemView.btnFavorite.setOnClickListener {
                    if(clickFavListener != null) clickFavListener!!(item)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if(loading) 1 else 0
    }

    fun isEmpty(): Boolean =
        itemCount == 0 || getItemViewType(0) == R.layout.item_loading

    override fun getItemViewType(position: Int): Int =
        if (position < super.getItemCount()) R.layout.item_character else R.layout.item_loading

    fun setLoading(loading: Boolean) {
        this.loading = loading
        notifyItemChanged(super.getItemCount())
    }

    fun updateItem(item: Character) {
        super.getCurrentList()?.forEachIndexed{
            index, character ->
                if(character.id == item.id) {
                    getItem(index)?.favorite = item.favorite
                    notifyItemChanged(index)
                    return
                }
        }
    }

    companion object {
        val diffCallback = object: DiffUtil.ItemCallback<Character>() {
            override fun areItemsTheSame(oldItem: Character?, newItem: Character?): Boolean {
                return newItem?.id == oldItem?.id
            }

            override fun areContentsTheSame(oldItem: Character?, newItem: Character?): Boolean {
                return newItem == oldItem
            }
        }
    }

}

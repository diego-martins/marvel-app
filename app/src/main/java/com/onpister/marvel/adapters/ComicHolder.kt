package com.onpister.marvel.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import com.onpister.marvel.data.models.Comic
import com.onpister.marvel.loadUrl
import kotlinx.android.synthetic.main.item_comic.view.*

class ComicHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(item: Comic) {
        itemView.textTitle.text = item.title
        val imgUrl = "${item.thumbnailPath}/standard_large.${item.thumbnailExtension}"
        itemView.imgThumb.loadUrl(imgUrl)
    }

}
package com.onpister.marvel.di

import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.dao.CharacterDao
import com.onpister.marvel.data.remote.ApiInterface
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ApiModule::class, DatabaseModule::class])
class RepoModule {

    @Singleton
    @Provides
    fun provideMarvelRepo(apiInterface: ApiInterface, characterDao: CharacterDao): MarvelRepo {
        return MarvelRepo(apiInterface, characterDao)
    }

}
package com.onpister.marvel.di

import android.app.Application
import com.onpister.marvel.BuildConfig
import com.onpister.marvel.Utils
import com.onpister.marvel.data.remote.APIClient
import com.onpister.marvel.data.remote.ApiInterface
import com.onpister.marvel.data.remote.NoConnectivityException
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        clientBuilder.readTimeout(30, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(30, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val logInterceptor = HttpLoggingInterceptor()
            logInterceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(logInterceptor)
        }

        clientBuilder.addInterceptor { chain ->

            if(!Utils.isOnline(application)){
                throw NoConnectivityException()
            }

            var request = chain.request()
            val newUrl = request.url().newBuilder()
                .addQueryParameter("ts", BuildConfig.apiTs)
                .addQueryParameter("apikey", BuildConfig.apiKey)
                .addQueryParameter("hash", BuildConfig.apiHash)
                .build()
            request = request.newBuilder().url(newUrl).build()
            chain.proceed(request)
        }
        return clientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideApiInterface(httpClient: OkHttpClient): ApiInterface {
        val apiClient = APIClient(httpClient)
        return apiClient.client.create(ApiInterface::class.java)
    }

}
package com.onpister.marvel.di

import android.app.Application
import com.onpister.marvel.activities.BaseApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: BaseApp) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }

}
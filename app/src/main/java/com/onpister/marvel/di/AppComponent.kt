package com.onpister.marvel.di

import com.onpister.marvel.activities.BaseApp
import com.onpister.marvel.activities.CharacterDetailActivity
import com.onpister.marvel.activities.MainActivity
import com.onpister.marvel.fragments.BaseFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(dependencies = [], modules = [
    AppModule::class,
    ApiModule::class,
    DatabaseModule::class,
    RepoModule::class,
    ViewModelModule::class
])
interface AppComponent {

    fun inject(application: BaseApp)

    fun inject(mainActivity: MainActivity)

    fun inject(baseFragment: BaseFragment)

    fun inject(characterDetailActivity: CharacterDetailActivity)

}
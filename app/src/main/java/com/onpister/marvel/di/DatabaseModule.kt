package com.onpister.marvel.di

import android.app.Application
import com.onpister.marvel.data.AppDatabase
import com.onpister.marvel.data.dao.CharacterDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private val application: Application) {

    private var appDatabase: AppDatabase = AppDatabase.getInstance(application)

    @Singleton
    @Provides
    fun provideAppDatabase(): AppDatabase {
        return appDatabase
    }

    @Singleton
    @Provides
    fun provideCharacterDao(appDatabase: AppDatabase): CharacterDao {
        return appDatabase.characterDao()
    }

}
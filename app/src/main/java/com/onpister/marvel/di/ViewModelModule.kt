package com.onpister.marvel.di

import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.viewmodels.CharactersViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RepoModule::class])
class ViewModelModule {

    @Singleton
    @Provides
    fun provideCharactersViewModelFactory(marvelRepo: MarvelRepo): CharactersViewModelFactory {
        return CharactersViewModelFactory(marvelRepo)
    }

}
package com.onpister.marvel.viewmodels.datasources

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PositionalDataSource
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.models.Character
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class CharacterDataSource(
    private val marvelRepo: MarvelRepo,
    private val compositeDisposable: CompositeDisposable
): PositionalDataSource<Character>() {

    private var searchText: String? = null
    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable>()

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Character>) {
        loading.postValue(true)
        error.postValue(null)

        val performCb = object: SingleObserver<List<Character>> {
            override fun onSuccess(result: List<Character>) {
                callback.onResult(result, 0, result.size)
                loading.postValue(false)
            }

            override fun onError(e: Throwable) {
                loading.postValue(false)
                error.postValue(e)
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }
        }

        if(searchText == null)
            marvelRepo.getAll(0, params.requestedLoadSize).subscribe(performCb)
        else
            marvelRepo.getAllBySearch(searchText!!, 0, params.requestedLoadSize).subscribe(performCb)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Character>) {
        loading.postValue(true)
        error.postValue(null)

        val performCb = object: SingleObserver<List<Character>> {
            override fun onSuccess(result: List<Character>) {
                callback.onResult(result)
                loading.postValue(false)
            }

            override fun onError(e: Throwable) {
                loading.postValue(false)
                error.postValue(e)
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }
        }

        if(searchText == null)
            marvelRepo.getAll(params.startPosition, params.loadSize).subscribe(performCb)
        else
            marvelRepo.getAllBySearch(searchText!!, params.startPosition, params.loadSize).subscribe(performCb)
    }

    fun setSearchText(searchText: String?) {
        this.searchText = searchText
    }
}
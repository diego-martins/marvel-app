package com.onpister.marvel.viewmodels

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.onpister.marvel.R
import com.onpister.marvel.data.*
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.data.models.FlexMessage
import com.onpister.marvel.data.models.Tabs
import com.onpister.marvel.viewmodels.datasources.CharacterDataSource
import com.onpister.marvel.viewmodels.datasources.CharacterDataSourceFactory
import com.onpister.marvel.viewmodels.datasources.FavoritesDataSource
import com.onpister.marvel.viewmodels.datasources.FavoritesDataSourceFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CharactersViewModel(
    private var marvelRepo: MarvelRepo
) : BaseViewModel() {

    private var currentTab = MutableLiveData<Tabs>()
    private var currentCharacter = MutableLiveData<Character>()
    private var characterList: LiveData<PagedList<Character>>
    private var favoriteList: LiveData<PagedList<Character>>
    private val compositeDisposable = CompositeDisposable()
    private var characterDataSourceFactory: CharacterDataSourceFactory
    private var favoriteDataSourceFactory: FavoritesDataSourceFactory
    private var error = MediatorLiveData<Throwable>()
    private val characterLoading: LiveData<Boolean>
    private val favoriteLoading: LiveData<Boolean>
    private var characterPending = false
    private var favoritePending = false
    private var isOnline = MutableLiveData<Boolean>()

    init {
        currentTab.value = Tabs.CHARACTERS

        characterDataSourceFactory =
                CharacterDataSourceFactory(marvelRepo, compositeDisposable)
        favoriteDataSourceFactory =
                FavoritesDataSourceFactory(marvelRepo, compositeDisposable)

        error.addSource(characterDataSourceFactory.getDataSource()) { it -> error.postValue(it?.error?.value) }
        error.addSource(favoriteDataSourceFactory.getDataSource()) { it -> error.postValue(it?.error?.value) }

        characterLoading = Transformations.switchMap(characterDataSourceFactory.getDataSource(), CharacterDataSource::loading )
        favoriteLoading = Transformations.switchMap(favoriteDataSourceFactory.getDataSource(), FavoritesDataSource::loading )

        val pagedConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(20)
            .setPageSize(20)
            .build()

        characterList = LivePagedListBuilder(characterDataSourceFactory, pagedConfig).build()
        favoriteList = LivePagedListBuilder(favoriteDataSourceFactory, pagedConfig).build()
    }

    fun invalidateCharacterDataSource() {
        characterPending = false
        characterDataSourceFactory.getDataSource().value?.invalidate()
    }

    fun invalidateFavoritesDataSource() {
        favoritePending = false
        favoriteDataSourceFactory.getDataSource().value?.invalidate()
    }

    fun invalidatePendingDataSource() {
        if(favoritePending) {
            invalidateFavoritesDataSource()
        }
        if(characterPending) {
            invalidateCharacterDataSource()
        }
    }

    fun searchText(searchText: String?, tab: Tabs = currentTab.value!!) {
        if(tab == Tabs.CHARACTERS) {
            if (characterDataSourceFactory.setSearchText(searchText)) {
                characterDataSourceFactory.getDataSource().value?.invalidate()
            }
        }
    }

    fun getCharacters(): LiveData<PagedList<Character>> {
        return characterList
    }

    fun getFavorites(): LiveData<PagedList<Character>> {
        return favoriteList
    }

    fun getError(): LiveData<Throwable> {
        return error
    }

    fun isLoadingCharacter(): LiveData<Boolean> {
        return characterLoading
    }

    fun isLoadingFavorite(): LiveData<Boolean> {
        return favoriteLoading
    }

    @SuppressLint("CheckResult")
    fun addFavorite(item: Character) {
        item.favorite = true
        marvelRepo.upsertFavorite(item).subscribeOn(Schedulers.io()).subscribe(
            { t: Long? ->
                if(t!! > 0) {
                    currentCharacter.postValue(item)
                    favoritePending = true
                    message.postValue(FlexMessage(R.string.favorite_added))
                }
            },
            { t: Throwable? ->
                message.postValue(FlexMessage(t?.message!!))
            }
        )
    }

    @SuppressLint("CheckResult")
    fun removeFavorite(item: Character) {
        item.favorite = false
        marvelRepo.deleteFavorite(item).subscribeOn(Schedulers.io()).subscribe(
            { t: Int? ->
                if(t!! > 0) {
                    when(currentTab.value) {
                        Tabs.FAVORITES -> {
                            characterPending = true
                            invalidateFavoritesDataSource()
                        }
                        Tabs.CHARACTERS -> {
                            favoritePending = true
                            // to improve perform
                            currentCharacter.postValue(item)
                        }
                    }
                    message.postValue(FlexMessage(R.string.favorite_removed))
                }
            },
            { t: Throwable? ->
                message.postValue(FlexMessage(t?.message!!))
            }
        )
    }

    fun getCurrentCharacter(): LiveData<Character> {
        return currentCharacter
    }

    fun setTab(tab: Tabs) {
        currentTab.value = tab
    }

    fun getTab(): LiveData<Tabs> {
        return currentTab
    }

    fun setOnline(isOnline: Boolean) {
        if(isOnline != this.isOnline.value) {
            this.isOnline.value = isOnline
        }
    }

    fun getOnline(): LiveData<Boolean> {
        return isOnline
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}

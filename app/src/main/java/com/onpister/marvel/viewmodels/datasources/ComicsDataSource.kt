package com.onpister.marvel.viewmodels.datasources

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PositionalDataSource
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.models.Comic
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class ComicsDataSource(
    private val characterId: Int,
    private val marvelRepo: MarvelRepo,
    private val compositeDisposable: CompositeDisposable
): PositionalDataSource<Comic>() {

    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable>()

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Comic>) {
        loading.postValue(true)
        error.postValue(null)

        val performCb = object: SingleObserver<List<Comic>> {
            override fun onSuccess(result: List<Comic>) {
                callback.onResult(result, 0, result.size)
                loading.postValue(false)
            }

            override fun onError(e: Throwable) {
                loading.postValue(false)
                error.postValue(e)
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }
        }

        marvelRepo.getAllComics(characterId, 0, params.requestedLoadSize).subscribe(performCb)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Comic>) {
        loading.postValue(true)
        error.postValue(null)

        val performCb = object: SingleObserver<List<Comic>> {
            override fun onSuccess(result: List<Comic>) {
                callback.onResult(result)
                loading.postValue(false)
            }

            override fun onError(e: Throwable) {
                loading.postValue(false)
                error.postValue(e)
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }
        }

        marvelRepo.getAllComics(characterId, params.startPosition, params.loadSize).subscribe(performCb)
    }

}
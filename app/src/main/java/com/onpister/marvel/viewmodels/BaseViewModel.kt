package com.onpister.marvel.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.onpister.marvel.data.models.FlexMessage

open class BaseViewModel : ViewModel() {
    private var refresh = MutableLiveData<Boolean>()
    protected var loading = MutableLiveData<Boolean>()
    private var finish = MutableLiveData<Boolean>()
    protected var message = MutableLiveData<FlexMessage>()

    init {
        refresh.value = false
        finish.value = false
    }

    fun setRefresh(visible: Boolean) {
        refresh.value = visible
    }

    fun getRefresh() : LiveData<Boolean> {
        return refresh
    }

    fun setFinish() {
        finish.value = true
    }

    fun postFinish() {
        finish.postValue(true)
    }

    fun getFinish(): LiveData<Boolean> {
        return finish
    }

    fun getMessage(): LiveData<FlexMessage> {
        return message
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }
}
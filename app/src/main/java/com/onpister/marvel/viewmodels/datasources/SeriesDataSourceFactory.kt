package com.onpister.marvel.viewmodels.datasources

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.models.Serie
import io.reactivex.disposables.CompositeDisposable

class SeriesDataSourceFactory(
    private val characterId: Int,
    private val marvelRepo: MarvelRepo,
    private val compositeDisposable: CompositeDisposable
): DataSource.Factory<Int, Serie>() {

    private val dataSource = MutableLiveData<SeriesDataSource>()

    override fun create(): DataSource<Int, Serie> {
        val seriesDataSource =
            SeriesDataSource(characterId, marvelRepo, compositeDisposable)
        dataSource.postValue(seriesDataSource)
        return seriesDataSource
    }

    fun getDataSource(): LiveData<SeriesDataSource> {
        return dataSource
    }
}
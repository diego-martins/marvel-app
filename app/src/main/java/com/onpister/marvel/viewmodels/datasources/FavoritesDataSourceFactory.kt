package com.onpister.marvel.viewmodels.datasources

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.models.Character
import io.reactivex.disposables.CompositeDisposable

class FavoritesDataSourceFactory(
    private val marvelRepo: MarvelRepo,
    private val compositeDisposable: CompositeDisposable
): DataSource.Factory<Int, Character>() {

    private val dataSource = MutableLiveData<FavoritesDataSource>()

    override fun create(): DataSource<Int, Character> {
        val characterDataSource =
            FavoritesDataSource(marvelRepo, compositeDisposable)
        dataSource.postValue(characterDataSource)
        return characterDataSource
    }

    fun getDataSource(): LiveData<FavoritesDataSource> {
        return dataSource
    }

}
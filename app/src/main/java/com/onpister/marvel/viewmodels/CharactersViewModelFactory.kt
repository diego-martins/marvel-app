package com.onpister.marvel.viewmodels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.onpister.marvel.data.MarvelRepo
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class CharactersViewModelFactory(
    private val marvelRepo: MarvelRepo
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(BaseViewModel::class.java) -> throw IllegalArgumentException("BaseViewModel is nos allowed to use")
        modelClass.isAssignableFrom(CharactersViewModel::class.java) -> CharactersViewModel(marvelRepo) as T
        modelClass.isAssignableFrom(CharacterDetailViewModel::class.java) -> CharacterDetailViewModel(marvelRepo) as T
        else -> throw IllegalArgumentException(modelClass::class.java.simpleName + " not found")
    }

}
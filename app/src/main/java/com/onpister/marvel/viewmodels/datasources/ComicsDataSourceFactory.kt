package com.onpister.marvel.viewmodels.datasources

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.models.Comic
import io.reactivex.disposables.CompositeDisposable

class ComicsDataSourceFactory(
    private val characterId: Int,
    private val marvelRepo: MarvelRepo,
    private val compositeDisposable: CompositeDisposable
): DataSource.Factory<Int, Comic>() {

    private val dataSource = MutableLiveData<ComicsDataSource>()

    override fun create(): DataSource<Int, Comic> {
        val comicDataSource =
            ComicsDataSource(characterId, marvelRepo, compositeDisposable)
        dataSource.postValue(comicDataSource)
        return comicDataSource
    }

    fun getDataSource(): LiveData<ComicsDataSource> {
        return dataSource
    }
}
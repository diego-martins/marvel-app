package com.onpister.marvel.viewmodels.datasources

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.models.Character
import io.reactivex.disposables.CompositeDisposable

class CharacterDataSourceFactory(
    private val marvelRepo: MarvelRepo,
    private val compositeDisposable: CompositeDisposable
): DataSource.Factory<Int, Character>() {

    private val dataSource = MutableLiveData<CharacterDataSource>()
    private var searchText: String? = null

    override fun create(): DataSource<Int, Character> {
        val characterDataSource =
            CharacterDataSource(marvelRepo, compositeDisposable)
        characterDataSource.setSearchText(searchText)
        dataSource.postValue(characterDataSource)
        return characterDataSource
    }

    fun setSearchText(searchText: String?): Boolean {
        if(searchText != this.searchText) {
            this.searchText = searchText
            return true
        }
        return false
    }

    fun getDataSource(): LiveData<CharacterDataSource> {
        return dataSource
    }

}
package com.onpister.marvel.viewmodels

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.onpister.marvel.R
import com.onpister.marvel.data.*
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.data.models.Comic
import com.onpister.marvel.data.models.FlexMessage
import com.onpister.marvel.data.models.Serie
import com.onpister.marvel.viewmodels.datasources.ComicsDataSource
import com.onpister.marvel.viewmodels.datasources.ComicsDataSourceFactory
import com.onpister.marvel.viewmodels.datasources.SeriesDataSource
import com.onpister.marvel.viewmodels.datasources.SeriesDataSourceFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CharacterDetailViewModel(
    private var marvelRepo: MarvelRepo
) : BaseViewModel() {

    private val character = MutableLiveData<Character>()
    private val compositeDisposable = CompositeDisposable()
    private val error = MediatorLiveData<Throwable>()
    private var originalFavorite = MutableLiveData<Boolean>()
    private lateinit var comicsDataSourceFactory: ComicsDataSourceFactory
    private lateinit var seriesDataSourceFactory: SeriesDataSourceFactory
    private var comicList: LiveData<PagedList<Comic>> = MutableLiveData<PagedList<Comic>>()
    private var serieList: LiveData<PagedList<Serie>> = MutableLiveData<PagedList<Serie>>()
    private var comicsLoading: LiveData<Boolean> = MutableLiveData<Boolean>()
    private var seriesLoading: LiveData<Boolean> = MutableLiveData<Boolean>()
    private var comicsError: LiveData<Throwable> = MutableLiveData<Throwable>()
    private var seriesError: LiveData<Throwable> = MutableLiveData<Throwable>()
    private var isOnline = MutableLiveData<Boolean>()

    init {
        loading.value = true
        originalFavorite.value = false
    }

    fun loadComics(characterId: Int) {
        comicsDataSourceFactory = ComicsDataSourceFactory(
            characterId,
            marvelRepo,
            compositeDisposable
        )
        comicsLoading = Transformations.switchMap(comicsDataSourceFactory.getDataSource(), ComicsDataSource::loading)
        comicsError = Transformations.switchMap(comicsDataSourceFactory.getDataSource(), ComicsDataSource::error)
        val pagedConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(20)
            .setPageSize(20)
            .build()
        comicList = LivePagedListBuilder(comicsDataSourceFactory, pagedConfig).build()
    }

    fun loadSeries(characterId: Int) {
        seriesDataSourceFactory = SeriesDataSourceFactory(
            characterId,
            marvelRepo,
            compositeDisposable
        )
        seriesLoading = Transformations.switchMap(seriesDataSourceFactory.getDataSource(), SeriesDataSource::loading)
        seriesError = Transformations.switchMap(seriesDataSourceFactory.getDataSource(), SeriesDataSource::error)
        val pagedConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(20)
            .setPageSize(20)
            .build()
        serieList = LivePagedListBuilder(seriesDataSourceFactory, pagedConfig).build()
    }

    fun getComicsLoading(): LiveData<Boolean> {
        return comicsLoading
    }

    fun getSeriesLoading(): LiveData<Boolean> {
        return seriesLoading
    }

    @SuppressLint("CheckResult")
    fun loadCharacter(id: Int) {
        loading.value = true
        marvelRepo.get(id).subscribeOn(Schedulers.io()).subscribe(
            { t: Character? ->
                if(t == null) {
                    message.postValue(FlexMessage(R.string.error_character_not_found))
                    postFinish()
                } else {
                    character.postValue(t)
                    originalFavorite.postValue(t.favorite)
                }
                loading.postValue(false)
            },
            { t: Throwable? ->
                error.postValue(t)
                loading.postValue(false)
                postFinish()
            }
        )
    }

    fun getCharacter(): LiveData<Character> {
        return character
    }

    fun getComics(): LiveData<PagedList<Comic>> {
        return comicList
    }

    fun invalidateComics() {
        comicsDataSourceFactory.getDataSource().value?.invalidate()
    }

    fun invalidateSeries() {
        seriesDataSourceFactory.getDataSource().value?.invalidate()
    }

    fun getSeries(): LiveData<PagedList<Serie>> {
        return serieList
    }

    fun getError(): LiveData<Throwable> {
        return error
    }

    fun getComicsError(): LiveData<Throwable> {
        return comicsError
    }

    fun getSeriesError(): LiveData<Throwable> {
        return seriesError
    }

    fun isFavoriteChanged(): Boolean {
        character.value?.let {
            return originalFavorite.value != it.favorite
        }
        return false
    }

    @SuppressLint("CheckResult")
    fun addFavorite() {
        character.value?.favorite = true
        marvelRepo.upsertFavorite(character.value!!).subscribeOn(Schedulers.io()).subscribe(
            { t: Long? ->
                if(t!! > 0) {
                    message.postValue(FlexMessage(R.string.favorite_added))
                    character.postValue(character.value)
                }
            },
            { t: Throwable? ->
                message.postValue(FlexMessage(t?.message!!))
                character.value?.favorite = false
            }
        )
    }

    @SuppressLint("CheckResult")
    fun removeFavorite() {
        character.value?.favorite = false
        marvelRepo.deleteFavorite(character.value!!).subscribeOn(Schedulers.io()).subscribe(
            { t: Int? ->
                if(t!! > 0) {
                    message.postValue(FlexMessage(R.string.favorite_removed))
                    character.postValue(character.value)
                }
            },
            { t: Throwable? ->
                message.postValue(FlexMessage(t?.message!!))
            }
        )
    }


    fun setOnline(isOnline: Boolean) {
        if(isOnline != this.isOnline.value) {
            this.isOnline.value = isOnline
        }
    }

    fun getOnline(): LiveData<Boolean> {
        return isOnline
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}

package com.onpister.marvel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.dao.CharacterDao
import com.onpister.marvel.data.models.Character
import com.onpister.marvel.data.remote.ApiInterface
import com.onpister.marvel.data.remote.ApiResponse
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MarvelRepoTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var api: ApiInterface

    @Mock
    lateinit var characterDao: CharacterDao

    @InjectMocks
    lateinit var marvelRepo: MarvelRepo

    @Test
    fun testGetFavorite() {
        val favorite = Mockito.mock(Character::class.java)
        favorite.id = 1
        favorite.favorite = true
        Mockito.`when`(characterDao.get(1)).thenReturn(Single.just(favorite))
        val actual = marvelRepo.getFavorite(1).blockingGet()
        assertEquals(favorite.id, actual.id)
        assertEquals(favorite.favorite, actual.favorite)
    }

    @Test
    fun testUpdateFavorite() {
        val favorite = Mockito.mock(Character::class.java)
        favorite.id = 1
        favorite.favorite = true
        Mockito.`when`(characterDao.insert(favorite)).thenReturn(0)
        Mockito.`when`(characterDao.update(favorite)).thenReturn(1)
        val actual = marvelRepo.upsertFavorite(favorite).blockingGet()
        assertEquals(1, actual)
    }

    @Test
    fun testDeleteFavorite() {
        val favorite = Mockito.mock(Character::class.java)
        favorite.id = 1
        favorite.favorite = true
        Mockito.`when`(characterDao.delete(favorite)).thenReturn(1)
        val actual = marvelRepo.deleteFavorite(favorite).blockingGet()
        assertEquals(1, actual)
    }

    @Test
    fun testGetAllWithoutFavorite() {
        val apiCharacters = ArrayList<ApiResponse.Character>()
        val ids = ArrayList<Int>()
        for(i in 1..20) {
            val characterItem = ApiResponse.Character()
            characterItem.id = i
            ids.add(i)
            apiCharacters.add(characterItem)
        }
        val apiResponse = ApiResponse.MarvelBase<ApiResponse.Character>()
        apiResponse.data.results = apiCharacters
        Mockito.`when`(api.getCharacters(0, 20)).thenReturn(Single.just(apiResponse))
        Mockito.`when`(characterDao.getAllByIds(ids)).thenReturn(Single.just(emptyList()))
        val actual = marvelRepo.getAll(0, 20).blockingGet()
        assertEquals(20, actual.size)
        assertEquals(1, actual[0].id)
        assertEquals(20, actual[19].id)
        assertEquals(false, actual[9].favorite)
    }

    @Test
    fun testGetAllWith3Favorite() {
        val apiCharacters = ArrayList<ApiResponse.Character>()
        val dbFavorites = ArrayList<Character>()
        val ids = ArrayList<Int>()
        for(i in 1..20) {
            val characterItem = ApiResponse.Character()
            characterItem.id = i
            ids.add(i)
            apiCharacters.add(characterItem)
        }
        for(i in 1..3) {
            val favoriteItem = Character()
            favoriteItem.id = i
            favoriteItem.favorite = true
            dbFavorites.add(favoriteItem)
        }
        val apiResponse = ApiResponse.MarvelBase<ApiResponse.Character>()
        apiResponse.data.results = apiCharacters
        Mockito.`when`(api.getCharacters(0, 20)).thenReturn(Single.just(apiResponse))
        Mockito.`when`(characterDao.getAllByIds(ids)).thenReturn(Single.just(dbFavorites))
        val actual = marvelRepo.getAll(0, 20).blockingGet()
        assertEquals(20, actual.size)
        assertEquals(true, actual[0].favorite)
        assertEquals(true, actual[1].favorite)
        assertEquals(true, actual[2].favorite)
        assertEquals(false, actual[3].favorite)
    }

}
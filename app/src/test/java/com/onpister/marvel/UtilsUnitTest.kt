package com.onpister.marvel

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.onpister.marvel.data.remote.NoConnectivityException
import junit.framework.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.net.SocketTimeoutException
import java.net.UnknownHostException

@RunWith(MockitoJUnitRunner::class)
class UtilsUnitTest {

    private fun getContextForNetwork(isOnline: Boolean): Context {
        val networkInfo = Mockito.mock(NetworkInfo::class.java)
        Mockito.`when`(networkInfo.isConnectedOrConnecting).thenReturn(isOnline)
        val cm = Mockito.mock(ConnectivityManager::class.java)
        Mockito.`when`(cm.activeNetworkInfo).thenReturn(networkInfo)
        val context = Mockito.mock(Context::class.java)
        Mockito.`when`(context.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(cm)
        return context
    }

    @Test
    fun testIsOnlineIsOnline() {
        val result = Utils.isOnline(getContextForNetwork(true))
        assertTrue(result)
    }

    @Test
    fun testIsOnlineIsOffline() {
        val result = Utils.isOnline(getContextForNetwork(false))
        assertFalse(result)
    }

    @Test
    fun testGetCustomErrorUnknownHostException() {
        val throwable = UnknownHostException()
        val result = Utils.getCustomError(throwable)
        assertEquals(R.string.error_server, result)
    }

    @Test
    fun testGetCustomErrorSocketTimeoutException() {
        val throwable = SocketTimeoutException()
        val result = Utils.getCustomError(throwable)
        assertEquals(R.string.error_timeout, result)
    }

    @Test
    fun testGetCustomErrorNoConnectivityException() {
        val throwable = NoConnectivityException()
        val result = Utils.getCustomError(throwable)
        assertEquals(R.string.error_offline, result)
    }

    @Test
    fun testGetCustomErrorOthersExceptions() {
        val throwable = Exception()
        val result = Utils.getCustomError(throwable)
        assertEquals(0, result)
    }

}
package com.onpister.marvel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.dao.CharacterDao
import com.onpister.marvel.data.models.Tabs
import com.onpister.marvel.data.remote.ApiInterface
import com.onpister.marvel.viewmodels.CharactersViewModel
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CharactersViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var api: ApiInterface

    @Mock
    lateinit var characterDao: CharacterDao

    @InjectMocks
    lateinit var marvelRepo: MarvelRepo

    val viewModel by lazy {
        CharactersViewModel(marvelRepo)
    }

    @Test
    fun testTabInit() {
        assert(viewModel.getTab().value == Tabs.CHARACTERS)
    }

    @Test
    fun testTabChangeToFavorites() {
        viewModel.setTab(Tabs.FAVORITES)
        assert(viewModel.getTab().value == Tabs.FAVORITES)
    }

    @Test
    fun testTabChangeToCharacters() {
        viewModel.setTab(Tabs.CHARACTERS)
        assert(viewModel.getTab().value == Tabs.CHARACTERS)
    }

}
package com.onpister.marvel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.onpister.marvel.data.MarvelRepo
import com.onpister.marvel.data.dao.CharacterDao
import com.onpister.marvel.data.remote.ApiInterface
import com.onpister.marvel.viewmodels.BaseViewModel
import com.onpister.marvel.viewmodels.CharacterDetailViewModel
import com.onpister.marvel.viewmodels.CharactersViewModel
import com.onpister.marvel.viewmodels.CharactersViewModelFactory
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.IllegalArgumentException

@RunWith(MockitoJUnitRunner::class)
class CharactersViewModelFactoryTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var api: ApiInterface

    @Mock
    lateinit var characterDao: CharacterDao

    @InjectMocks
    lateinit var marvelRepo: MarvelRepo

    @Test
    fun testFactoryToCharactersViewModel(){
        val viewModel = CharactersViewModelFactory(marvelRepo)
        val result = viewModel.create(CharactersViewModel::class.java)
        assertEquals(CharactersViewModel::class.java, result::class.java)
    }

    @Test
    fun testFactoryToCharacterDetailViewModel(){
        val viewModel = CharactersViewModelFactory(marvelRepo)
        val result = viewModel.create(CharacterDetailViewModel::class.java)
        assertEquals(CharacterDetailViewModel::class.java, result::class.java)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testFactoryToUndefinedViewModel(){
        val viewModel = CharactersViewModelFactory(marvelRepo)
        viewModel.create(BaseViewModel::class.java)
    }
}
# Marvel App

## Desafio

- [Conforme solicitado](https://github.com/jjfernandes87/Challenge/blob/master/README_Android_Senior.md)

## Requisitos Essenciais

- [Conforme solicitado](https://github.com/jjfernandes87/Challenge/blob/master/README_Android_Senior.md)

## Bônus

- Coverage com Jacoco
- RxJava
- Extension Functions
- Dagger2
- Mockito
- Espresso
- Extenssion Functions

## Outros

- Architecture MVVM
- Test build variant
- BroadcastReceiver
- Room
- Paging
- Retrofit2
- MockWebService
- Repository Pattern
- [Publicado na Play Store (Beta)](https://play.google.com/apps/testing/com.onpister.marvel)
